/*!
* Design by: 
* URI: 
* Year: 2020
* Coder: Chris
*
* timestamp: 25th - 01 - 2020
*/

function consoleSecurityAlert()
{
	console.log('%cStop!', 'background: transparent; color: red; font-size: 40px;');console.log('%cThis is a browser feature intended for developers. If someone told you to copy-paste something here to enable a feature or "hack" someone\'s account, it is a scam and will give them access to your accounts.', 'background: transparent; color: green; font-size: 16px;');
}

function sliderHTMLfive(a)
{
	$(a).each(function()
	{
		slides   = $(this).find('> div');
		slidnu   = slides.length;
		anispeed = $(this).data('mspeed');

		slides.first().addClass('first active'); // Adding first class
		slides.css('display', 'none'); // Display none for all slides
		slides.first().css('display', 'block'); // Display block first slide
		slides.last().addClass('end'); // Adding last class

		if (!slides.hasClass('slid'))
		{
			slides.addClass('slid');
		}

		slides.each(function() // Each slide
		{
			$(this).addClass('sld'+($(this).index()+1));
		});

		if ($(this).data('controls') == true)
		{
			$(this).prepend('<button class="left"> < </button><button class="right"> > </button>');
		}

		if ($(this).data('circles') == true)
		{
			$(this).append('<div class="circles"></div>');
		}

		for (var i = 0; i < slidnu; i++)
		{
			$(this).find('div.circles').append('<button class="inl" data-display="sld'+(i+1)+'">'+(i+1)+'</button>');
		}

		function animationSlide(a)
		{
			$(a).find('> div.slid').fadeOut(anispeed);
			$(a).find('> div.slid.active').stop().delay(anispeed+50).fadeIn(anispeed);
		}

		$(this).find('button.right').click(function()
		{
			if ($(this).parents(a).find('> div.active').hasClass('end'))
			{
				$(this).parents(a).find('> div.active').removeClass('active');
				$(this).parents(a).find('> div.first').addClass('active');

				$(this).parents(a).find('> div.circles button').removeClass('active');
				$(this).parents(a).find('> div.circles button:first-child').addClass('active');

				animationSlide(a);

				return false;
			}

			$(this).parents(a).find('> div.active').next().addClass('active');
			$(this).parents(a).find('> div.active').prev().removeClass('active');

			$(this).parents(a).find('> div.circles button.active').next().addClass('active');
			$(this).parents(a).find('> div.circles button.active').prev().removeClass('active');

			animationSlide(a);
		});

		$(this).find('button.left').click(function()
		{
			if ($(this).parents(a).find('> div.active').hasClass('first'))
			{
				$(this).parents(a).find('> div.active').removeClass('active');
				$(this).parents(a).find('> div.end').addClass('active');

				$(this).parents(a).find('> div.circles button').removeClass('active');
				$(this).parents(a).find('> div.circles button:last-child').addClass('active');

				animationSlide(a);

				return false;
			}

			$(this).parents(a).find('> div.active').prev().addClass('active');
			$(this).parents(a).find('> div.active').next().removeClass('active');

			$(this).parents(a).find('> div.circles button.active').prev().addClass('active');
			$(this).parents(a).find('> div.circles button.active').next().removeClass('active');

			animationSlide(a);
		});

		circleButton = $(this).find('.circles button');

		circleButton.first().addClass('active');

		circleButton.click(function()
		{
			data = $(this).data('display');

			if (!$(this).hasClass('active'))
			{
				$(this).parents(a).find('> div').removeClass('active');
				$(this).parents(a).find('> div.'+data).addClass('active');

				$(this).parents(a).find('div.circles button').removeClass('active');
				$(this).addClass('active');

				animationSlide(a);
			}
		});

		if ($(this).data('autoplay') == true)
		{
			$(a).find('> button.right').addClass('autoplay');

			function playIt()
			{
				$(a).find('> button.right.autoplay').trigger('click');
			}

			setInterval(playIt, 5000);

			$(a).hover(function()
			{
				$(a).find('> button.right').removeClass('autoplay');
			}, function() {
				$(a).find('> button.right').addClass('autoplay');
			});
		}
	});
}